<?php

class seculo {
   
    public function RetornaSeculo($ano) {
        // Pega os dois últimos digitos do ano 
        $doisUltimosDigitos = substr($ano, -2);

        // Compara se os dois últimos digitos do ano são iguais a 00, Caso o contrário pega os dois primeiros digitos e soma mais um  
        if ($doisUltimosDigitos == '00') {
            $seculo = substr($ano, 0, 2);
        } else {
            $seculo = substr($ano, 0, 2);
            $seculo = $seculo + 1;
        }
        return $seculo;
    }

}
