<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Função que Retorna o Século</title>
    </head>
    <body>
        <?php
        require_once 'seculo.php';
        
        $retornaSeculo = new seculo();
        $seculo = '';

        if (isset($_POST['btnCalcular'])) {
            if ($_POST['ano'] != '') {
                $seculo = $retornaSeculo->RetornaSeculo($_POST['ano']);
            } else {
                echo "<script type='text/javascript'>alert('Digite um ano para calcular o século');</script>";
            }
        }
        ?>
        <form action="index.php" method="POST">
            <label>Digite o Ano:</label>
            <input name="ano">
            <label>O Século é:</label>
            <label name="seculo"><?= $seculo ?></label>
            <button name = "btnCalcular">Calcular</button>
        </form>
    </body>
</html>
